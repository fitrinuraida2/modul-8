import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class StandartInput {
	public static void main(String[]args) throws IOException {
		System.out.println("Hi, what's your favorit character?");
		char favChar;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		favChar = (char)br.read();
		System.out.println(favChar + " is a good choice!");
	}
}
