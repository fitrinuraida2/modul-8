 import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.NoSuchElementException;
import java.util.Formatter;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.*;

public class Tugas extends JFrame implements ActionListener{

	JTextArea display=new JTextArea();
	JButton read=new JButton("Baca File");
	JButton write=new JButton("Tulis File");
	JButton make=new JButton("Tambah");
	
	JTextField name=new JTextField(10);
	JPanel PanelAtas=new JPanel();
	JPanel PanelBawah=new JPanel();
	private static Formatter output;
	
	public Tugas()
	{
		super("Tugas Modul 8");
		read.addActionListener(this);
		write.addActionListener(this);
		make.addActionListener(this);
		
		PanelBawah.setLayout(new GridLayout(1,3,1,1));
		PanelAtas.add(name);
		PanelAtas.add(make);
		PanelBawah.add(read);
		PanelBawah.add(write);
		display.setLineWrap(true);
		this.setLayout(new BorderLayout());
		this.add(new JScrollPane(display));
		this.add(PanelAtas, BorderLayout.NORTH);
		this.add(display,BorderLayout.CENTER);
		this.add(PanelBawah, BorderLayout.SOUTH);
	}
	
	private void readTextFile(JTextArea display, String fileName)
	{
		try
		{
			BufferedReader inStream=new BufferedReader(new FileReader(fileName));
			String line=inStream.readLine();
			while(line!=null)
			{
				display.append(line + "\n");
				line=inStream.readLine();
			}
			inStream.close();
		}
		catch (FileNotFoundException e) 
		{
			display.setText("IO Error:  " + fileName + " Not Found. "+"\n");
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			display.setText("IO Error: " + e.getMessage() +"\n");
			e.printStackTrace();
		}
	}
	
	private void writeTextFile(JTextArea display, String fileName)
	{
		try
		{
		FileWriter ostream=new FileWriter(fileName);
		ostream.write(display.getText());
		JOptionPane.showMessageDialog(null,"Data Dimasukan Dalam File", "Perintah",JOptionPane.INFORMATION_MESSAGE);
		ostream.close();
		}
		catch(IOException e)
		{
			display.setText("IO Error:  " + fileName + " Not Found. "+"\n");
			e.printStackTrace();
			
		}
		
	}
	
	private void MakeTextFile(JTextArea display, String fileName)
	{
		try 
		{
			output = new Formatter(fileName);
			JOptionPane.showMessageDialog(null,"File Dibuat", "Perintah",JOptionPane.INFORMATION_MESSAGE);
		} catch (SecurityException se) {
			System.err.println("Write permission denied. Terminating.");
			System.exit(1);
		} catch (FileNotFoundException fe) {
			System.err.println("Error opening file. Terminating.");
			System.exit(1);
		}
	}
	
	public void actionPerformed(ActionEvent ae) 
	{
		String fileName=name.getText();
		if(ae.getSource()==read)
		{
			display.setText("");
			readTextFile(display,fileName);
		}
		else if(ae.getSource()==write)
		{
			writeTextFile(display, fileName);
		}
		else if(ae.getSource()==make)
		{
			MakeTextFile(display, fileName);
		}
	}
	
	public static void main(String[] args)
	{
			Tugas object=new Tugas();
			object.setSize(800,400);
		
			object.setVisible(true);
			object.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
}
